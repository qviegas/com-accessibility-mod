# com-accessibility-mod

Ensemble de modules destinés à améliorer l'accessibilité de différents
composants Drupal.

## Modules

### bm_webform_accessibility

Module permettant d'améliorer l'accessibilité du module Webform.
